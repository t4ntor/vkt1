﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace vkt1
{
    class Program
    {
        static void Main(string[] args)
        {
            RealTimeCityBikeDataFetcher newFetch = new RealTimeCityBikeDataFetcher();

            Console.WriteLine("Hei! Minkä pyöräparkin tiedot haluat?\n");
            string userIn = Console.ReadLine();

           newFetch.GetBikeCountInStation(userIn).Wait();

           Console.WriteLine("Pyöräparkissa"+userIn+"on vapaana {GetBikeCountInStation()} pyörää.");
        }
    }

    public class Station
    {
        public int id;
        public string name;
        public int x;
        public int y;
        public int bikesAvailable;
        public int spacesAvailable;
        public bool allowDropoff;
        public string networks;
        public bool realTimeData;
    }
    public class StationList  //To save Json data
    {
        public List<Station> bdata;
    }


   
    public interface ICityBikeDataFetcher
    {
        Task<int> GetBikeCountInStation(string stationName);

    }

    public class RealTimeCityBikeDataFetcher : ICityBikeDataFetcher
    {
       // const int MAX_BUFFER_SIZE = 2048;
        static Encoding enc8 = Encoding.UTF8;
            public async Task<int> GetBikeCountInStation(string stationName)
        {

            string checkStatus = stationName;
            int bcount;
            int i = 0;
            System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
            HttpResponseMessage bikedat = await client.GetAsync("http://api.digitransit.fi/routing/v1/routers/hsl/bike_rental");
            byte[] bytes = null;

            bikedat.Content = new ByteArrayContent(content: bytes);
            string fbytes = enc8.GetString(bytes);
            StationList realTime = new StationList();
            realTime = JsonConvert.DeserializeObject<StationList>(fbytes);

            while (realTime.bdata[i].name != stationName)
            {
                i = i++;
            }

            bcount = realTime.bdata[i].bikesAvailable;


            return bcount;
        }
    }
}